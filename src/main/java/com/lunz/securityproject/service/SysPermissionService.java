package com.lunz.securityproject.service;

import com.lunz.securityproject.entity.domain.SysPermission;

import java.util.List;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 16:45
 */
public interface SysPermissionService {
    /**
     * 查询用户的权限列表
     * @param userId
     * @return
     */
    List<SysPermission> selectListByUser(Integer userId);

    /**
     * 查询具体某个接口的权限
     *
     * @param path
     * @return
     */
    List<SysPermission> selectListByPath(String path);
}
