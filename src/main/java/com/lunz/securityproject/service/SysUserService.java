package com.lunz.securityproject.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lunz.securityproject.entity.domain.SysUser;
import com.lunz.securityproject.entity.vo.RegisterUser;
import com.lunz.securityproject.entity.vo.UpdatePassword;


/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 16:38
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * 通过账号查找
     * @param userName 用户账号
     * @return
     */
    SysUser selectByName(String userName);

    /**
     * 更新用户信息并返回新的用户信息
     * @param sysUser
     * @return
     */
    SysUser update(SysUser sysUser);

    /**
     * 用户注册
     * @param user
     * @return
     */
    int insert(RegisterUser user);

    /**
     * 修改密码
     * @param account
     * @param updatePassword
     * @return
     */
    int updatePassword(String account, UpdatePassword updatePassword);

    /**
     * 删除用户
     * @param account
     * @return
     */
    int deleteUser(String account);
}
