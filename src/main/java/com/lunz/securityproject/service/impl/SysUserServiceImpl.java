package com.lunz.securityproject.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lunz.securityproject.entity.domain.SysUser;
import com.lunz.securityproject.entity.domain.UserAndRoleRelation;
import com.lunz.securityproject.entity.vo.RegisterUser;
import com.lunz.securityproject.entity.vo.UpdatePassword;
import com.lunz.securityproject.mapper.SysUserMapper;
import com.lunz.securityproject.mapper.UserAndRoleRelationMapper;
import com.lunz.securityproject.service.SysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 17:08
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserService service;

    @Autowired
    private UserAndRoleRelationMapper userAndRoleRelationMapper;

    @Override
    public SysUser selectByName(String userName) {
        return baseMapper.selectByName(userName);
    }

    @Override
    public SysUser update(SysUser sysUser) {
        baseMapper.updateById(sysUser);
        return baseMapper.selectById(sysUser.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(RegisterUser user) {
        //判断账号是否重复
        int count = service.lambdaQuery().select(SysUser :: getId).eq(SysUser :: getAccount, user.getAccount()).count();
        if(count != 0){
            return 0;
        }
        //增加用户
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        sysUser.setPassword(bCryptPasswordEncoder.encode(sysUser.getPassword()));
        sysUser.setLastLoginTime(new Date());
        sysUser.setEnabled(true);
        sysUser.setAccountNonExpired(true);
        sysUser.setAccountNonLocked(true);
        sysUser.setCredentialsNonExpired(true);
        sysUser.setCreateTime(new Date());
        sysUser.setUpdateTime(new Date());
        baseMapper.insert(sysUser);
        //获取新增用户id并在用户-角色表中增加记录
        SysUser u = service.lambdaQuery().select(SysUser :: getId).eq(SysUser :: getAccount, sysUser.getAccount()).one();
        UserAndRoleRelation userAndRoleRelation = new UserAndRoleRelation();
        userAndRoleRelation.setUserId(u.getId());
        userAndRoleRelation.setRoleId(2);
        return userAndRoleRelationMapper.insert(userAndRoleRelation);
    }

    @Override
    public int updatePassword(String account, UpdatePassword updatePassword) {
        //获取数据库中的账号密码
        SysUser sysUser = service.lambdaQuery().select(SysUser::getAccount, SysUser :: getPassword).eq(SysUser::getAccount, account).one();
        //oldPassword为数据库中存的密码
        String oldPassword = sysUser.getPassword();
        //调用matches方法将未加密的密码与经过加密的密码比对是否相同（第一个参数为未加密密码，第二个为加密密码）
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        boolean b = bCryptPasswordEncoder.matches(updatePassword.getOldPassword(), oldPassword);
        //密码正确则修改
        if(b){
            updatePassword.setAccount(account);
            updatePassword.setNewPassword(bCryptPasswordEncoder.encode(updatePassword.getNewPassword()));
            return  baseMapper.updatePassword(updatePassword);
        }
        return -1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteUser(String account) {
        //删除用户-角色表中的记录
        int id = userAndRoleRelationMapper.getIdByAccount(account);
        userAndRoleRelationMapper.deleteById(id);
        return baseMapper.deleteUser(account);
    }
}
