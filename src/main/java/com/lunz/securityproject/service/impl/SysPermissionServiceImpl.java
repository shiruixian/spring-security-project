package com.lunz.securityproject.service.impl;

import com.lunz.securityproject.entity.domain.SysPermission;
import com.lunz.securityproject.mapper.SysPermissionMapper;
import com.lunz.securityproject.service.SysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 17:10
 */
@Service
public class SysPermissionServiceImpl implements SysPermissionService {

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public List<SysPermission> selectListByUser(Integer userId) {
        return sysPermissionMapper.selectListByUser(userId);
    }

    @Override
    public List<SysPermission> selectListByPath(String path) {
        return sysPermissionMapper.selectListByPath(path);
    }
}
