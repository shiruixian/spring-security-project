package com.lunz.securityproject.mapper;

import com.lunz.securityproject.entity.domain.SysPermission;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 16:42
 */
@Repository
public interface SysPermissionMapper {
    /**
     * 根据用户名查询用户的权限信息
     * @param userId
     * @return
     */
    List<SysPermission> selectListByUser(Integer userId);

    /**
     * 查询具体某个接口的权限
     *
     * @param path 接口路径
     * @return
     */
    List<SysPermission> selectListByPath(String path);
}
