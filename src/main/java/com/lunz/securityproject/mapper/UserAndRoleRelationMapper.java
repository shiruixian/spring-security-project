package com.lunz.securityproject.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lunz.securityproject.entity.domain.UserAndRoleRelation;
import org.springframework.stereotype.Repository;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/10/29 16:02
 */
@Repository
public interface UserAndRoleRelationMapper extends BaseMapper<UserAndRoleRelation> {

    /**
     * 增加
     * @param roleAndPermision
     * @return
     */
    @Override
    int insert(UserAndRoleRelation roleAndPermision);

    /**
     * 通过account获取id
     * @param account
     * @return
     */
    int getIdByAccount(String account);
}
