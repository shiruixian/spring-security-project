package com.lunz.securityproject.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lunz.securityproject.entity.domain.SysUser;
import com.lunz.securityproject.entity.vo.UpdatePassword;
import org.springframework.stereotype.Repository;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 16:33
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 通过账号获取用户
     * @param userName 用户账号
     * @return
     */
    SysUser selectByName(String userName);

    /**
     * 修改密码
     * @param updatePassword
     * @return
     */
    int updatePassword(UpdatePassword updatePassword);

    /**
     * 删除用户
     * @param account
     * @return
     */
    int deleteUser(String account);
}
