package com.lunz.securityproject.entity.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/10/29 10:04
 */
@Getter
@Setter
@ToString
public class Vaptcha {
    private String id;
    private String secretkey;
    private Integer scene;
    private String token;
    private String ip;
    private String userid;
    public Vaptcha() {
        // TODO Auto-generated constructor stub
    }
    public Vaptcha(String id, String secretkey, Integer scene, String token, String ip, String userid) {
        super();
        this.id = id;
        this.secretkey = secretkey;
        this.scene = scene;
        this.token = token;
        this.ip = ip;
        this.userid = userid;
    }
}
