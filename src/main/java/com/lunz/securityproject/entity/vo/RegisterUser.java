package com.lunz.securityproject.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/10/22 14:23
 */
@Data
@ToString
@ApiModel(value = "用户注册")
public class RegisterUser implements Serializable {

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;
    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String account;


    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * vaptcha验证token
     */
    @ApiModelProperty(value = "vaptcha验证token")
    private String token;

    /**
     * vaptcha验证server
     */
    @ApiModelProperty(value = "vaptcha验证server")
    private String server;

}
