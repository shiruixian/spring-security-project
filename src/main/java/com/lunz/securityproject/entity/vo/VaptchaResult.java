package com.lunz.securityproject.entity.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/10/29 10:07
 */
@Getter
@Setter
@ToString
public class VaptchaResult {
    private String msg;
    private Integer score;
    private Integer success;
    public VaptchaResult() {
        // TODO Auto-generated constructor stub
    }
    public VaptchaResult(String msg, Integer score, Integer success) {
        super();
        this.msg = msg;
        this.score = score;
        this.success = success;
    }
}
