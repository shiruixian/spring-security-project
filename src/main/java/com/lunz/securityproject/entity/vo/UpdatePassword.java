package com.lunz.securityproject.entity.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/10/29 13:43
 */
@Getter
@Setter
@ToString
public class UpdatePassword {
    private String account;
    private String oldPassword;
    private String newPassword;
}
