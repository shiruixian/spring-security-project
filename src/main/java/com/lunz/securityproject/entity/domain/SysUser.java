package com.lunz.securityproject.entity.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 16:34
 */
@Data
@TableName("sys_user")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 915478504870211231L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //账号
    @TableField(value = "account")
    private String account;

    //用户名
    @TableField(value = "user_name")
    private String userName;

    //用户密码
    @TableField(value = "password")
    private String password;

    //上一次登录时间
    @TableField(value = "last_login_time")
    private Date lastLoginTime;

    //账号是否可用。默认为1（可用）
    @TableField(value = "enabled")
    private Boolean enabled;

    //是否过期。默认为1（没有过期）
    @TableField(value = "account_not_expired")
    private Boolean accountNonExpired;

    //账号是否锁定。默认为1（没有锁定）
    @TableField(value = "account_not_locked")
    private Boolean accountNonLocked;

    //证书（密码）是否过期。默认为1（没有过期）
    @TableField(value = "credentials_not_expired")
    private Boolean credentialsNonExpired;

    //创建时间
    @TableField(value = "create_time")
    private Date createTime;
    //修改时间
    @TableField(value = "update_time")
    private Date updateTime;
}
