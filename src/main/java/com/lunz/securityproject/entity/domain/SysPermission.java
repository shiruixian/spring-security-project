package com.lunz.securityproject.entity.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author 施瑞贤
 * @email shiruixian@zhongruigroup.com
 * @date 2021/11/5 16:40
 */
@Data
@TableName("sys_permission")
public class SysPermission implements Serializable {
    private static final long serialVersionUID = -71969734644822184L;

    //主键id
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //权限code
    @TableField(value = "permission_code")
    private String permissionCode;

    //权限名
    @TableField(value = "permission_name")
    private String permissionName;
}
